extern crate csv;
extern crate serde;
#[macro_use]
extern crate serde_derive;

use std::env;
use std::error::Error;
use std::process;

#[derive(Debug, Deserialize, Clone)]
struct Book {
    name: String,
    author: String,
    genre: String,
    gross_sales: f64,
}

fn read_books_from_csv(file_path: &str) -> Result<Vec<Book>, Box<dyn Error>> {
    let mut rdr = csv::Reader::from_path(file_path)?;
    let mut books = Vec::new();
    for result in rdr.deserialize() {
        let book: Book = result?;
        books.push(book);
    }
    Ok(books)
}

fn total_gross_sales(books: &[Book]) -> f64 {
    books.iter().map(|book| book.gross_sales).sum()
}

fn filter_books_by_genre(books: &[Book], genre: &str) -> Vec<Book> {
    books
        .iter()
        .filter(|book| book.genre == genre)
        .cloned()
        .collect()
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = "books.csv";
    let books = read_books_from_csv(file_path).unwrap_or_else(|err| {
        println!("Error reading CSV file: {}", err);
        process::exit(1);
    });

    if args.len() > 1 {
        let genre = &args[1];
        let filtered_books = filter_books_by_genre(&books, genre);
        println!("Found {} {} books.", filtered_books.len(), genre);
        for book in filtered_books.iter().take(10) { // Limiting to the first 10 books for brevity
            println!("\"{}\" by {}", book.name, book.author);
        }
    } else {
        if let Some(first_book) = books.first() {
            println!("First book: \"{}\" by {}", first_book.name, first_book.author);
        }

        println!("Total Gross Sales: ${:.2}", total_gross_sales(&books));
        
        let fiction_books = filter_books_by_genre(&books, "fiction");
        println!("Found {} fiction books.", fiction_books.len());
    }
}