[![pipeline status](https://gitlab.com/hollyyfc/hollycui_vector/badges/main/pipeline.svg)](https://gitlab.com/hollyyfc/hollycui_vector/-/commits/main)

# 🗃️ Holly's Data Processing with Vector Database

## Description

This project implements a [Rust Vector Database](https://docs.rs/vectordb/latest/vectordb/) structure to perform data processing and analysis on a dataset of books. The application reads a CSV file (`books.csv`*) containing book information, including titles, authors, genres, and gross sales figures, then efficiently filters and aggregates this data based on user-specified criteria, such as genre. With functionalities to calculate total gross sales and filter books by genre, the project demonstrates an approach to data manipulation and querying within a vector-based storage model. 

## Demo 

- **Default Test**

When using `cargo run`, the default operations will be invoked and return general information about the full dataset, including the first presented book's name and author, the total gross sales of all books in the dataset, and the number of books with a genre of fiction:

![default](https://gitlab.com/hollyyfc/hollycui_vector/-/wikis/uploads/ba6cd4c6c7739a6b7da006d73eee7cb3/default.png)

- **Test with Specified Argument**

The functions also allow users to specify detailed arguments upon calling, which will yield filtered output. 

When using `cargo run fiction`, the functions will return the top 10 books belonging to the genre of fiction ordered by gross sales:

![fiction](https://gitlab.com/hollyyfc/hollycui_vector/-/wikis/uploads/f0d4123789beff45244acf03a71d204c/fiction.png)

When using `cargo run nonfiction`, the functions will return the top 10 books belonging to the genre of nonfiction ordered by gross sales:

![nonfiction](https://gitlab.com/hollyyfc/hollycui_vector/-/wikis/uploads/a133bc3644a40b44f998c72c9fb24767/nonfiction.png)

You can always specify other types of genre for customized results. 

## Steps Walkthrough

- `cargo new <PROJECT-NAME>` in desired directory
- Add `serde`, `serde_json`, `serde_derive` and `csv` to `Cargo.toml`
- Build Rust functions in `src/main.rs` using `Vector` database structure
- `cargo build`
- `cargo run` or with specific arguments 

## Detailed Structure

- **Book Structure**

Define the structure of each book instance based on available variables in our CSV file:
```rust
struct Book {
    name: String,
    author: String,
    genre: String,
    gross_sales: f64,
}
```
Each row in the file will then be read as individual `Book` instance following the structure. 

- **Filtering & Aggregation**

Filter and aggregate functions are defined to select books from specific genres and calculate gross book sales:
```rust
fn total_gross_sales(books: &[Book]) -> f64 {
    books.iter().map(|book| book.gross_sales).sum()
}

fn filter_books_by_genre(books: &[Book], genre: &str) -> Vec<Book> {
    books
        .iter()
        .filter(|book| book.genre == genre)
        .cloned()
        .collect()
}
```

- **Output Conditions**

Inside the `main()` function, we detail the conditions of `cargo run`, where two different sets of output are considered based on the length of the input arguments: 

```rust
if args.len() > 1 {   // with filter on genre
    let genre = &args[1];
    let filtered_books = filter_books_by_genre(&books, genre);
    println!("Found {} {} books.", filtered_books.len(), genre);
    for book in filtered_books.iter().take(10) {
        println!("\"{}\" by {}", book.name, book.author);
    }
} else {   // without filter, simply aggregate
    if let Some(first_book) = books.first() {
        println!("First book: \"{}\" by {}", first_book.name, first_book.author);
    }
    println!("Total Gross Sales: ${:.2}", total_gross_sales(&books));
    let fiction_books = filter_books_by_genre(&books, "fiction");
    println!("Found {} fiction books.", fiction_books.len());
}
```

## Reference

*The dataset `books.csv` is retrieved from dataset [Books Sales and Ratings](https://www.kaggle.com/datasets/thedevastator/books-sales-and-ratings) on Kaggle and subsequently cleaned by me for project use. 